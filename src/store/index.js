import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import images from './images.js';
export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    images,
  }
})
