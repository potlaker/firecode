
export default {
    namespaced: true,
    state: {
        listImages: [],
        imagePopUp: null,
        skeletonStatus: true,
    },
    getters: {
        getImages(state){
            // возвращает массив с изображениями
            return state.listImages;
        },
        getImagePopup(state){
            // возвпащает одно изображение для popup
            return state.imagePopUp;
        },
        getSkeletonStatus(state){
            // возвращает активатор скелетона
            return state.skeletonStatus;
        },
    },
    mutations: {
        setImages(state, data){
            // добаляет изображение в массив
            if(Array.isArray(data)){
                state.listImages.push(...data);
            }else{
                state.listImages.push(data);
            }
        },
        deleteImage(state, index){
            // удаляет изображение из массива по индексу
            state.listImages.splice(index, 1);
        },
        setImagePopup(state, data){
            state.imagePopUp = data;
            // console.log(state.imagePopUp)

        },
        setSkeletonStatus(state, status){
            // устанавливает активатор скелетона
            state.skeletonStatus = status;
        },
    },
    actions: {
        putImages({commit}, data){
            // добавляет изображения в store из localStorage
            commit('setImages', data);
        },
        deleteImage({commit}, index){
            // удаляет изображения из store и localStorage
            commit('deleteImage', index);
            let images = JSON.parse( localStorage.getItem('images') );
            images.splice(index, 1);
            localStorage.setItem( 'images', JSON.stringify(images) );
        },
        putImagePopup({commit}, data){
            // добавляет изображение в popup
            commit('setImagePopup', data);
        },
        putSkeletonStatus({commit}, status){
            // переключает скелетон на status
            commit('setSkeletonStatus', status);
        },
    }
}