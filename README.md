# firecode

## Описание тестового заания для FireCode
```
Использованы: vue2, css(чистый), javascript
Задание в файле "задание.txt"
```
```
Задания выполнены все, включая доп. задания.
Украшениями не занимался.
Проверку и вывод ошибок также не реализовывал.
Все остальное как в примере на видео и в доп. пунктах.
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
